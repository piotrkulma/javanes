package piotrkulma.javanes;

import piotrkulma.javanes.cpu.CPU;
import piotrkulma.javanes.nesrom.file.inesrom.INesRomData;
import piotrkulma.javanes.nesrom.reader.NesRomReader;
import piotrkulma.javanes.nesrom.reader.inesrom.INesRomReader;

import java.io.IOException;


public class Main {
    public static void main(String... args) throws IOException {
        NesRomReader<INesRomData> reader = INesRomReader.getInstance();
        INesRomData iNesRomData = reader.readNesFile("xxx.y");

        CPU cpu = new CPU();
        cpu.loadProgram(iNesRomData.getPrgBanksData(), iNesRomData.getHeader().getPrgRomBanksSize());

        int a = 0b0;
        int b = 0b1;
        int c = a - b;
        System.out.println(Integer.toBinaryString(c));
        //while(true) {
            //cpu.tick();
        //}
    }
}
