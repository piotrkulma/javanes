package piotrkulma.javanes.common;

import piotrkulma.javanes.cpu.memory.Bit;

import java.io.IOException;
import java.io.InputStream;

public final class BinaryUtils {
    //256
    public static final int VALUE_0x0100 = 0x0100;
    public static final int VALUE_0x000F = 0x000F;
    public static final int VALUE_0x00FF = 0x00FF;
    public static final int VALUE_0x0008 = 0x0008;

    public static byte[] readRawBytes(InputStream is, int count) throws IOException {
        byte[] ba = new byte[count];

        is.read(ba);
        return ba;
    }

    public static int[] readBytes(InputStream is, int count) throws IOException {
        byte[] ba = new byte[count];

        is.read(ba);
        return convertToUnsignedInteger(ba);
    }

    public static int getBinarySize(int b) {
        int index = 0;
        String binaryString = Integer.toBinaryString(b);
        for(int i=0; i<binaryString.length(); i++) {
            if(BinaryUtils.getBit(b, i) == 1) {
                index = (i + 1);
            }
        }

        return index;
    }

    public static int getHByte(int b) {
        return (b >> 4) & VALUE_0x000F;
    }

    public static int getLByte(int b) {
        return (b  & VALUE_0x000F);
    }

    public static int getBit(int b, int num) {
        return (b & 1 << num) >> num;
    }

    public static Bit getBit(int b) {
        if(b == 0) {
            return Bit.B0;
        } else if(b == 1) {
            return Bit.B1;
        }

        throw new RuntimeException("Bit must be 0 or 1 but found: " + b);
    }

    public static int normalizeByteValue(int a) {
        return normalizeValue(a, VALUE_0x0100);
    }

    public static int normalizeValue(int a, int max) {
        return a % max;
    }

    public static int formTwoBytesValue(int lb, int hb) {
        return normalizeByteValue(hb) << VALUE_0x0008 | normalizeByteValue(lb);
    }

    public static int[] convertToUnsignedInteger(byte b[]) {
        int[] ib = new int[b.length];

        for(int i=0; i<ib.length; i++) {
            ib[i] = convertToUnsignedInteger(b[i]);
        }

        return ib;
    }

    public static int convertToUnsignedInteger(byte b) {
        return b & VALUE_0x00FF;
    }

    private BinaryUtils() {
    }
}
