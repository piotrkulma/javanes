package piotrkulma.javanes.cpu.opcode;

public enum AddressingMode {
    ACC,
    IMPL,
    REL,
    IMM,
    IND,
    INDX,
    INDY,
    ZPG,
    ZPGX,
    ZPGY,
    ABS,
    ABSX,
    ABSY;
}
