package piotrkulma.javanes.cpu.opcode;

public class OperationCodeData {
    private int bytes;

    private String flags;
    private String cycles;
    private OperationCode operationCode;
    private AddressingMode addressingMode;

    public OperationCodeData() {
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public String getCycles() {
        return cycles;
    }

    public void setCycles(String cycles) {
        this.cycles = cycles;
    }

    public int getBytes() {
        return bytes;
    }

    public void setBytes(int bytes) {
        this.bytes = bytes;
    }

    public OperationCode getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(OperationCode operationCode) {
        this.operationCode = operationCode;
    }

    public AddressingMode getAddressingMode() {
        return addressingMode;
    }

    public void setAddressingMode(AddressingMode addressingMode) {
        this.addressingMode = addressingMode;
    }

    @Override
    public String toString() {
        return "OperationCodeData{" +
                "operationCode=" + operationCode +
                ", addressingMode=" + addressingMode +
                ", flags=" + flags +
                ", bytes=" + bytes +
                ", cycles=" + cycles +
                '}';
    }
}
