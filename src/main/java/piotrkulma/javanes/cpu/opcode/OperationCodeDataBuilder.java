package piotrkulma.javanes.cpu.opcode;

public class OperationCodeDataBuilder {
    private int bytes;

    private String flags;
    private String cycles;
    private OperationCode operationCode;
    private AddressingMode addressingMode;

    public OperationCodeData build() {
        OperationCodeData data = new OperationCodeData();
        data.setFlags(flags);
        data.setCycles(cycles);
        data.setBytes(bytes);
        data.setOperationCode(operationCode);
        data.setAddressingMode(addressingMode);

        return data;
    }

    public OperationCodeDataBuilder withBytes(int bytes) {
        this.bytes = bytes;
        return this;
    }

    public OperationCodeDataBuilder withFlags(String flags) {
        this.flags = flags;
        return this;
    }

    public OperationCodeDataBuilder withCycles(String cycles) {
        this.cycles = cycles;
        return this;
    }

    public OperationCodeDataBuilder withOperationCode(OperationCode operationCode) {
        this.operationCode = operationCode;
        return this;
    }

    public OperationCodeDataBuilder withAddressingMode(AddressingMode addressingMode) {
        this.addressingMode = addressingMode;
        return this;
    }
}
