package piotrkulma.javanes.cpu.opcode.executor.data;

public class OperationResolvedData {
    private boolean accumulator;
    private boolean value;
    private boolean address;
    private boolean implied;
    private int resolvedValue;

    public boolean isAccumulator() {
        return accumulator;
    }

    public void setAccumulator(boolean accumulator) {
        this.accumulator = accumulator;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public boolean isAddress() {
        return address;
    }

    public void setAddress(boolean address) {
        this.address = address;
    }

    public boolean isImplied() {
        return implied;
    }

    public void setImplied(boolean implied) {
        this.implied = implied;
    }

    public int getResolvedValue() {
        return resolvedValue;
    }

    public void setResolvedValue(int resolvedValue) {
        this.resolvedValue = resolvedValue;
    }
}
