package piotrkulma.javanes.cpu.opcode.executor;

import piotrkulma.javanes.common.BinaryUtils;
import piotrkulma.javanes.cpu.memory.Flag;
import piotrkulma.javanes.cpu.memory.Memory;

import static piotrkulma.javanes.cpu.memory.Flag.*;

public final class Utils {
    public static boolean isAffectedFlag(Flag flag, String affectedFlags) {
        return affectedFlags.contains(flag.name());
    }

    public static void affectsFlags(Memory memory, String flags, int sum) {
        if(isAffectedFlag(N, flags)) {
            affectsNegativeFlag(memory, sum);
        }

        if(isAffectedFlag(V, flags)) {
            affectsOverflowFlag(memory, sum);
        }

        if(isAffectedFlag(N, flags)) {
            affectsZeroFlag(memory, sum);
        }

        if(isAffectedFlag(C, flags)) {
            affectsCarryFlag(memory, sum);
        }
    }

    private static void affectsNegativeFlag(Memory memory, int sum) {
        if (BinaryUtils.getBit(sum, 7) == 1) {
            memory.setProcesorStatusByte(N);
        } else {
            memory.clearProcesorStatusByte(N);
        }
    }

    private static void affectsOverflowFlag(Memory memory, int sum) {
        if (sum > 127 || BinaryUtils.getBinarySize(sum) > 8) {
            memory.setProcesorStatusByte(V);
        } else {
            memory.clearProcesorStatusByte(V);
        }
    }

    private static void affectsZeroFlag(Memory memory, int sum) {
        if (sum == 0) {
            memory.setProcesorStatusByte(Z);
        } else {
            memory.clearProcesorStatusByte(Z);
        }
    }

    private static void affectsCarryFlag(Memory memory, int sum) {
        if(sum > 0xFF) {
            memory.setProcesorStatusByte(C);
        } else {
            memory.clearProcesorStatusByte(C);
        }
    }


    private Utils() {
    }
}
