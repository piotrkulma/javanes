package piotrkulma.javanes.cpu.opcode.executor;

import org.apache.log4j.Logger;
import piotrkulma.javanes.cpu.memory.Memory;
import piotrkulma.javanes.cpu.opcode.OperationCodeData;
import piotrkulma.javanes.cpu.opcode.executor.data.OperationResolvedData;

public  final class OperationCodeExecutor {
    final static Logger LOG = Logger.getLogger(OperationCodeExecutor.class);

    private static OperationCodeExecutor INSTANCE;
    private OperationImplementation operationImpl;
    private OperationAddressingResolver addrResolver;

    public static OperationCodeExecutor getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new OperationCodeExecutor();
        }

        return INSTANCE;
    }

    public void execute(Memory memory, OperationCodeData operationCodeData, int[] arguments) {
        LOG.debug(operationCodeData);
        for(int i=0; i<arguments.length; i++) {
            LOG.debug(" ARG["+i+"]:" + arguments[i]);
        }

        OperationResolvedData data = addrResolver.resolve(memory, operationCodeData.getAddressingMode(), arguments);

        executeOperationImpl(memory, operationCodeData, data);
    }

    private void executeOperationImpl(Memory memory, OperationCodeData operationCodeData, OperationResolvedData data) {
        switch (operationCodeData.getOperationCode()) {
            case ADC:
                operationImpl.adc(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case AND:
                operationImpl.and(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case ASL:
                operationImpl.asl(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BCC:
                operationImpl.bcc(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BCS:
                operationImpl.bcs(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BEQ:
                operationImpl.beq(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BIT:
                operationImpl.bit(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BMI:
                operationImpl.bmi(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BNE:
                operationImpl.bne(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BPL:
                operationImpl.bpl(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BRA:
                operationImpl.bra(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BRK:
                operationImpl.brk(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BVC:
                operationImpl.bvc(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case BVS:
                operationImpl.bvs(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case CLC:
                operationImpl.clc(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case CLD:
                operationImpl.cld(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case CLI:
                operationImpl.cli(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case CLV:
                operationImpl.clv(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case CMP:
                operationImpl.cmp(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case CPX:
                operationImpl.cpx(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case CPY:
                operationImpl.cpy(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case DEC:
                operationImpl.dec(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case DEX:
                operationImpl.dex(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case DEY:
                operationImpl.dey(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case EOR:
                operationImpl.eor(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case INC:
                operationImpl.inc(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case INX:
                operationImpl.inx(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case INY:
                operationImpl.iny(memory, data, operationCodeData.getAddressingMode(), operationCodeData.getFlags());
                break;
            case JMP: break;
            case JSR: break;
            case LDA: break;
            case LDX: break;
            case LDY: break;
            case LSR: break;
            case NOP: break;
            case ORA: break;
            case PHA: break;
            case PHP: break;
            case PLA: break;
            case PLP: break;
            case ROL: break;
            case ROR: break;
            case RTI: break;
            case RTS: break;
            case SBC: break;
            case SEC: break;
            case SED: break;
            case SEI: break;
            case STA: break;
            case STX: break;
            case STY: break;
            case TAX: break;
            case TAY: break;
            case TSX: break;
            case TXA: break;
            case TXS: break;
            case TYA: break;
            default: throw new RuntimeException("Operation " + operationCodeData.getOperationCode() + " is not implemented.");
        }
    }

    private OperationCodeExecutor() {
        addrResolver = OperationAddressingResolver.getInstance();
        operationImpl = OperationImplementation.getInstance();
    }
}
