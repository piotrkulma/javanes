package piotrkulma.javanes.cpu.opcode.executor.builder;

import piotrkulma.javanes.cpu.opcode.executor.data.OperationResolvedData;

public final class OperationResolvedDataBuilder {
    private boolean accumulator;
    private boolean value;
    private boolean address;
    private boolean implied;
    private int resolvedValue;

    public static OperationResolvedDataBuilder getInstance() {
        return new OperationResolvedDataBuilder();
    }

    public OperationResolvedData build() {
        OperationResolvedData data = new OperationResolvedData();
        data.setAccumulator(accumulator);
        data.setValue(value);
        data.setAddress(address);
        data.setResolvedValue(resolvedValue);
        data.setImplied(implied);

        return data;
    }

    public OperationResolvedDataBuilder withAccumulator(boolean accumulator) {
        this.accumulator = accumulator;
        return this;
    }

    public OperationResolvedDataBuilder withValue(boolean value) {
        this.value = value;
        return this;
    }

    public OperationResolvedDataBuilder withAddress(boolean address) {
        this.address = address;
        return this;
    }

    public OperationResolvedDataBuilder withImplied(boolean implied) {
        this.implied = implied;
        return this;
    }

    public OperationResolvedDataBuilder withResolvedValue(int resolvedValue) {
        this.resolvedValue = resolvedValue;
        return this;
    }

    private OperationResolvedDataBuilder() {

    }
}
