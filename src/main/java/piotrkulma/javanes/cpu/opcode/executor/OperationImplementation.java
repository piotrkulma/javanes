package piotrkulma.javanes.cpu.opcode.executor;

import piotrkulma.javanes.common.BinaryUtils;
import piotrkulma.javanes.cpu.memory.Memory;
import piotrkulma.javanes.cpu.opcode.AddressingMode;
import piotrkulma.javanes.cpu.opcode.executor.data.OperationResolvedData;

import static piotrkulma.javanes.cpu.opcode.executor.Utils.*;

import static piotrkulma.javanes.cpu.memory.Flag.*;

import static piotrkulma.javanes.cpu.opcode.AddressingMode.*;

import static piotrkulma.javanes.common.BinaryUtils.*;

public final class OperationImplementation {
    private static OperationImplementation INSTANCE;

    public static OperationImplementation getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new OperationImplementation();
        }

        return INSTANCE;
    }

    public void adc(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);
        int res = m + memory.getA() + memory.getProcesorStatusByte(C);

        affectsFlags(memory, flags, res);
        memory.setA(res);
    }

    public void and(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);
        int res = m & memory.getA();

        affectsFlags(memory, flags, res);
        memory.setA(res);
    }

    public void asl(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);
        int res = m << 1;

        affectsFlags(memory, flags, res);
        if(mode == ACC) {
            memory.setA(res);
        } else {
            memory.setMemoryValue(data.getResolvedValue(), res);
        }
    }

    public void bcc(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);

        if(memory.getProcesorStatusByte(C) == 0) {
            memory.setPc(m);
        }
    }

    public void bcs(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);

        if(memory.getProcesorStatusByte(C) == 1) {
            memory.setPc(m);
        }
    }

    public void beq(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);

        if(memory.getProcesorStatusByte(Z) == 1) {
            memory.setPc(m);
        }
    }

    public void bit(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);
        int res = memory.getA() & m;

        affectsFlags(memory, flags, res);
        memory.setProcesorStatusByte(V, getBit(BinaryUtils.getBit(res, 6)));
        memory.setProcesorStatusByte(N, getBit(BinaryUtils.getBit(res, 7)));
    }

    public void bmi(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);

        if(memory.getProcesorStatusByte(N) == 1) {
            memory.setPc(m);
        }
    }

    public void bne(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);

        if(memory.getProcesorStatusByte(Z) == 0) {
            memory.setPc(m);
        }
    }

    public void bpl(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);

        if(memory.getProcesorStatusByte(N) == 0) {
            memory.setPc(m);
        }
    }

    public void bra(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);
        memory.setPc(m);
    }

    public void brk(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);
        memory.setPc(m);
        memory.clearProcesorStatusByte(I);
        memory.setProcesorStatusByte(D);
        memory.clearProcesorStatusByte(B);
    }

    public void bvc(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);

        if(memory.getProcesorStatusByte(V) == 0) {
            memory.setPc(m);
        }
    }

    public void bvs(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);

        if(memory.getProcesorStatusByte(V) == 1) {
            memory.setPc(m);
        }
    }

    public void clc(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        memory.clearProcesorStatusByte(C);
    }

    public void cld(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        memory.clearProcesorStatusByte(D);
    }

    public void cli(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        memory.clearProcesorStatusByte(I);
    }

    public void clv(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        memory.clearProcesorStatusByte(V);
    }

    public void cmp(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        compare(memory, memory.getA(), getResolvedValue(memory, data));
    }

    public void cpx(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        compare(memory, memory.getX(), getResolvedValue(memory, data));
    }

    public void cpy(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        compare(memory, memory.getY(), getResolvedValue(memory, data));
    }

    public void dec(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);
        int res = m - 1;

        affectsFlags(memory, flags, res);
        memory.setMemoryValue(data.getResolvedValue(), res);
    }

    public void dex(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int res = memory.getX() - 1;

        affectsFlags(memory, flags, res);
        memory.setX(res);
    }

    public void dey(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int res = memory.getY() - 1;

        affectsFlags(memory, flags, res);
        memory.setY(res);
    }

    //TODO all
    public void eor(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
    }

    public void inc(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int m = getResolvedValue(memory, data);
        int res = m + 1;

        affectsFlags(memory, flags, res);
        memory.setMemoryValue(data.getResolvedValue(), res);
    }

    public void inx(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int res = memory.getX() + 1;

        affectsFlags(memory, flags, res);
        memory.setX(res);
    }

    public void iny(Memory memory, OperationResolvedData data, AddressingMode mode, String flags) {
        int res = memory.getY() + 1;

        affectsFlags(memory, flags, res);
        memory.setY(res);
    }

    //http://6502.org/tutorials/compare_instructions.html
    private void compare(Memory memory, int a, int b) {
        if(a < b) {
            memory.clearProcesorStatusByte(Z);
            memory.clearProcesorStatusByte(C);
        } else if(a == b) {
            memory.clearProcesorStatusByte(N);
            memory.setProcesorStatusByte(Z);
            memory.setProcesorStatusByte(C);
        } else if(a > b) {
            memory.clearProcesorStatusByte(Z);
            memory.setProcesorStatusByte(C);
        }
    }

    private int getResolvedValue(Memory memory, OperationResolvedData data) {
        int m = 0;

        if(data.isValue()) {
            m = data.getResolvedValue();
        } else if(data.isAccumulator()) {
            m = memory.getA();
        } else if(data.isAddress()) {
            m = memory.getMemoryValue(data.getResolvedValue());
        } else if(!data.isImplied()){
            throw new RuntimeException("Operation data are corrupted: " + data);
        }

        return m;
    }

    private OperationImplementation() {
    }
}
