package piotrkulma.javanes.cpu.opcode.executor;

import piotrkulma.javanes.cpu.memory.Memory;
import piotrkulma.javanes.cpu.opcode.AddressingMode;
import piotrkulma.javanes.cpu.opcode.executor.builder.OperationResolvedDataBuilder;
import piotrkulma.javanes.cpu.opcode.executor.data.OperationResolvedData;

import static piotrkulma.javanes.common.BinaryUtils.*;

public final class OperationAddressingResolver {
    private static OperationAddressingResolver INSTANCE;

    public static OperationAddressingResolver getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new OperationAddressingResolver();
        }

        return INSTANCE;
    }

    public OperationResolvedData resolve(Memory memory, AddressingMode addressingMode, int[] arguments) {
        OperationResolvedData data = null;

        switch (addressingMode) {
            case ABS:
                data = OperationResolvedDataBuilder.getInstance()
                        .withAddress(true)
                        .withResolvedValue(formTwoBytesValue(arguments[0], arguments[1]))
                        .build();
                break;
            case IMM:
                data = OperationResolvedDataBuilder.getInstance()
                        .withValue(true)
                        .withResolvedValue(arguments[0])
                        .build();
                break;
            case ACC:
                data = OperationResolvedDataBuilder.getInstance()
                        .withAccumulator(true)
                        .build();
                break;
            case REL:
                data = OperationResolvedDataBuilder.getInstance()
                        .withValue(true)
                        .withResolvedValue(arguments[0])
                        .build();
                break;
            case IND:
                break;
            case INDX:
                break;
            case INDY:
                break;
            case ZPG:
                data = OperationResolvedDataBuilder.getInstance()
                        .withAddress(true)
                        .withResolvedValue(normalizeByteValue(arguments[0]))
                        .build();
                break;
            case ABSX:
                data = OperationResolvedDataBuilder.getInstance()
                        .withAddress(true)
                        .withResolvedValue(formTwoBytesValue(arguments[0], arguments[1]) + memory.getX())
                        .build();
                break;
            case ABSY:
                data = OperationResolvedDataBuilder.getInstance()
                        .withAddress(true)
                        .withResolvedValue(formTwoBytesValue(arguments[0], arguments[1]) + memory.getY())
                        .build();
                break;
            case IMPL:
                data = OperationResolvedDataBuilder.getInstance()
                        .withImplied(true)
                        .build();
                break;
            case ZPGX:
                data = OperationResolvedDataBuilder.getInstance()
                        .withAddress(true)
                        .withResolvedValue(normalizeByteValue(arguments[0] + memory.getX()))
                        .build();
                break;
            case ZPGY:
                data = OperationResolvedDataBuilder.getInstance()
                        .withAddress(true)
                        .withResolvedValue(normalizeByteValue(arguments[0] + memory.getY()))
                        .build();
                break;
            default:
                throw new RuntimeException("AddressingMode "+ addressingMode+" is not implemenmted" );
        }

        return data;
    }

    private OperationAddressingResolver() {
    }
}
