package piotrkulma.javanes.cpu.opcode.decoder;

import piotrkulma.javanes.Main;
import piotrkulma.javanes.common.BinaryUtils;
import piotrkulma.javanes.cpu.opcode.AddressingMode;
import piotrkulma.javanes.cpu.opcode.OperationCode;
import piotrkulma.javanes.cpu.opcode.OperationCodeData;
import piotrkulma.javanes.cpu.opcode.OperationCodeDataBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class OperationCodeDecoder {
    private static final String NES_OP_CODES_SPLITTER = ";";
    private static final String NES_OP_CODES_INTERNAL_SPLITTER = "\\|";
    private static final String NES_OP_CODES_FILE_NAME = "OP_CODES.txt";

    private static final int OP_CODE_INDEX      = 0;
    private static final int ADDR_MODE_INDEX    = 1;
    private static final int BYTES_INDEX        = 2;
    private static final int CYCLES_INDEX       = 3;
    private static final int FLAGS_INDEX        = 4;


    private static OperationCodeDecoder INSTANCE;
    public static OperationCodeData[][] opCodeMatrix;

    public static OperationCodeDecoder getInstance() throws IOException {
        if(INSTANCE == null) {
            initialize();
        }

        return INSTANCE;
    }

    public OperationCodeData decode(int opNumber) {
        int hb = BinaryUtils.getHByte(opNumber);
        int lb = BinaryUtils.getLByte(opNumber);
        OperationCodeData operationCodeData = opCodeMatrix[hb][lb];

        if(operationCodeData == null) {
            throw new RuntimeException("Operation not found: " + Integer.toHexString(opNumber));
        }
        return operationCodeData;
    }

    private static void initialize() throws IOException {
        INSTANCE = new OperationCodeDecoder();
        initializeOpCodeMatrix();
    }

    private static void initializeOpCodeMatrix() throws IOException {
        opCodeMatrix = new OperationCodeData[16][16];

        try(InputStream is = Main.class.getClassLoader().getResourceAsStream(NES_OP_CODES_FILE_NAME);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is))) {
            fetchOpCodeMatrix(bufferedReader);
        }
    }

    private static void fetchOpCodeMatrix(BufferedReader bufferedReader) throws IOException {
        int i = 0, j;
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            j = 0;
            String[] opCodes = line.split(NES_OP_CODES_SPLITTER, 0);
            for (String opCode : opCodes) {
                String[] opCodeData = opCode.split(NES_OP_CODES_INTERNAL_SPLITTER, 0);
                if (opCodeData.length > 1) {
                    opCodeMatrix[i][j] = getOperationCodeData(opCodeData);
                }
                j++;
            }
            i++;
        }
    }

    private static OperationCodeData getOperationCodeData(String[] opCodeData) {
        OperationCodeDataBuilder builder = new OperationCodeDataBuilder();
        builder.withOperationCode(OperationCode.valueOf(opCodeData[OP_CODE_INDEX]))
                .withAddressingMode(AddressingMode.valueOf(opCodeData[ADDR_MODE_INDEX]))
                .withBytes(Integer.valueOf(opCodeData[BYTES_INDEX]))
                .withFlags(opCodeData[FLAGS_INDEX])
                .withCycles(opCodeData[CYCLES_INDEX]);

        return builder.build();
    }

    private OperationCodeDecoder() {
    }
}
