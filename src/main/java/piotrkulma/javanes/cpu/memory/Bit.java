package piotrkulma.javanes.cpu.memory;

public enum Bit {
    B0(0), B1(0);

    private int value;

    Bit(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
