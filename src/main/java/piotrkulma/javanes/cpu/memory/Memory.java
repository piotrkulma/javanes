package piotrkulma.javanes.cpu.memory;

import java.util.Arrays;

import static piotrkulma.javanes.common.BinaryUtils.*;

public class Memory {
    //2^16 = 65536 = 0x10000
    public static final int CPU_MEMORY_SIZE = 0x10000;

    public static final int ZERO_PAGE_BEGIN_ADDR            = 0x0000;
    public static final int STACK_BEGIN_ADDR                = 0x0100;
    public static final int RAM_BEGIN_ADDR                  = 0x0200;
    public static final int MIRRORS_LOWER_BEGIN_ADDR        = 0x0800;
    public static final int IO_REGISTERS_LOWER_BEGIN_ADDR   = 0x2000;
    public static final int MIRRORS_UPPER_BEGIN_ADDR        = 0x2008;
    public static final int IO_REGISTERS_UPPER_BEGIN_ADDR   = 0x4000;
    public static final int EXPANSION_ROM_BEGIN_ADDR        = 0x4020;
    public static final int SRAM_BEGIN_ADDR                 = 0x6000;
    public static final int PRG_ROM_LOWER_BEGIN_ADDR        = 0x8000;
    public static final int PRG_ROM_UPPER_BEGIN_ADDR        = 0xC000;

    private int[] memory;
    private int pc;
    private int sp;
    private int a;
    private int x;
    private int y;
    private int[] p;

    public Memory() {
        initRegisters();
        initMemory();
    }

    public void setMemoryValue(int address, int value) {
        //value must be one byte value from range 0x00 - 0xFF (0-255)
        //if(value > 0xFF) {
            //throw new RuntimeException("Attempt to write:'" + value +"' into memory addres:" + address +" Memory value must be from range 0-255.");
        //}

        value = normalizeByteValue(value);
        memory[address] = value;

        if(isLowerMirroredSector(address)) {
            fillMirroredAdresses(0x0800, 0x2000, 0x0000, 2048, address, value);
        }

        if(isUpperMirroredSector(address)) {
            fillMirroredAdresses(0x2008, 0x4000, 0x2000, 8, address, value);
        }
    }

    private void fillMirroredAdresses(int mirrorBegin, int mirrorEnd, int mirroredSectorBegin, int mirroredSectorLen, int address, int value) {
        int offset = address - mirroredSectorBegin;
        for(int i=mirrorBegin + offset; i<mirrorEnd; i+=mirroredSectorLen) {
            memory[i] = value;
        }
    }

    private boolean isLowerMirroredSector(int addr) {
        return addr>=ZERO_PAGE_BEGIN_ADDR && addr<MIRRORS_LOWER_BEGIN_ADDR;
    }

    private boolean isUpperMirroredSector(int addr) {
        return addr>=IO_REGISTERS_LOWER_BEGIN_ADDR && addr<MIRRORS_UPPER_BEGIN_ADDR;
    }

    public int getMemoryValue(int address) {
        return memory[address];
    }

    public int getPc() {
        return pc;
    }

    public void setPc(int pc) {
        this.pc = normalizeByteValue(pc);
    }

    public void incPc() {
        intPc(1);
    }

    public void intPc(int val) {
        this.pc = this.pc + val;
    }

    public int getSp() {
        return sp;
    }

    public void setSp(int sp) {
        this.sp = normalizeByteValue(sp);
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = normalizeByteValue(a);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = normalizeByteValue(x);
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = normalizeByteValue(y);
    }

    public void setProcesorStatusByte(Flag flag) {
        p[flag.getIndex()] = 1;
    }

    public void setProcesorStatusByte(Flag flag, Bit bit) {
        if(bit == Bit.B0) {
            clearProcesorStatusByte(flag);
        } else {
            setProcesorStatusByte(flag);
        }
    }

    public void clearProcesorStatusByte(Flag flag) {
        p[flag.getIndex()] = 0;
    }

    public int getProcesorStatusByte(Flag flag) {
        return p[flag.getIndex()];
    }

    private void initRegisters() {
        p = new int[]{0, 0, 1, 0, 0, 0, 0, 0};
    }

    private void initMemory() {
        memory = new int[CPU_MEMORY_SIZE];

        for(int i=0; i<CPU_MEMORY_SIZE; i++) {
            memory[i] = 0x0000;
        }
    }

    @Override
    public String toString() {
        return "Memory{" +
                "pc=" + pc +
                ", sp=" + sp +
                ", a=" + a +
                ", x=" + x +
                ", y=" + y +
                ", p=" + Arrays.toString(p) +
                '}';
    }
}
