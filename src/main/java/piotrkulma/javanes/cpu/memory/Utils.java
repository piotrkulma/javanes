package piotrkulma.javanes.cpu.memory;

public final class Utils {
    public static void loadProgramIntoMemory(Memory memory, int[][] program, int banks) {
        if(banks == 1) {
            loadLowerBank(memory, program[0]);
            loadUpperBank(memory, program[0]);
        }

        if(banks == 2) {
            loadLowerBank(memory, program[0]);
            loadUpperBank(memory, program[1]);
        }
    }

    private static void loadLowerBank(Memory memory, int[] bank) {
        copyDataIntoMemoryRange(memory, bank, 0x8000, 0xC000 - 1);
    }

    private static void loadUpperBank(Memory memory, int[] bank) {
        copyDataIntoMemoryRange(memory, bank, 0xC000, 0x10000 - 1);
    }

    private static void copyDataIntoMemoryRange(Memory memory, int[] data, int startAddr, int endAddr) {
        int dataIndex = 0;
        for(int i=startAddr; i<=endAddr; i++) {
            memory.setMemoryValue(i, data[dataIndex++]);
        }
    }

    private Utils() {
    }
}
