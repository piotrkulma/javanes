package piotrkulma.javanes.cpu.memory;

public enum Flag {
    N(7), V(6), B(4), D(3), I(2), Z(1), C(0);

    private int index;

    Flag(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
