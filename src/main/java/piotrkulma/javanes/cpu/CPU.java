package piotrkulma.javanes.cpu;

import org.apache.log4j.Logger;
import piotrkulma.javanes.cpu.memory.Memory;
import piotrkulma.javanes.cpu.memory.Utils;
import piotrkulma.javanes.cpu.opcode.OperationCodeData;
import piotrkulma.javanes.cpu.opcode.decoder.OperationCodeDecoder;
import piotrkulma.javanes.cpu.opcode.executor.OperationCodeExecutor;

import java.io.IOException;

public class CPU {
    final static Logger LOG = Logger.getLogger(CPU.class);

    private Memory memory;
    private OperationCodeDecoder decoder;
    private OperationCodeExecutor executor;

    public CPU() throws IOException {
        memory = new Memory();
        memory.setPc(0x8000);
        decoder = OperationCodeDecoder.getInstance();
        executor = OperationCodeExecutor.getInstance();
    }

    public void loadProgram(int[][] program, int banks) {
        Utils.loadProgramIntoMemory(memory, program, banks);
    }

    public void tick() {
        LOG.debug("MEMORY BEFORE TICK:" + memory);

        int opcode = memory.getMemoryValue(memory.getPc());
        memory.incPc();

        OperationCodeData operationCodeData = decoder.decode(opcode);

        int[] arguments = fetchInstructionArguments(operationCodeData);
        executor.execute(memory, operationCodeData, arguments);

        LOG.debug("MEMORY AFTER TICK:" + memory);
    }

    private int[] fetchInstructionArguments(OperationCodeData operationCodeData) {
        int[] arguments = new int[operationCodeData.getBytes()-1];

        if(operationCodeData.getBytes() > 1) {
            for(int i=0; i<arguments.length; i++) {
                arguments[i] = memory.getMemoryValue(memory.getPc());
                memory.incPc();
            }
        }

        return arguments;
    }
}
