package piotrkulma.javanes.nesrom.builder;

import piotrkulma.javanes.nesrom.file.NesRomData;

public interface NesRomDataBuilder<T extends NesRomData> {
    T build();
}
