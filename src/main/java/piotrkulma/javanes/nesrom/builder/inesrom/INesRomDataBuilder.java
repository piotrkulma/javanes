package piotrkulma.javanes.nesrom.builder.inesrom;

import piotrkulma.javanes.nesrom.builder.NesRomDataBuilder;
import piotrkulma.javanes.nesrom.file.inesrom.INesRomData;
import piotrkulma.javanes.nesrom.file.inesrom.INesRomDataHeader;

public class INesRomDataBuilder  implements NesRomDataBuilder<INesRomData> {
    private INesRomDataHeader header;
    private int[] trainerData;
    private int[][] prgBanksData;
    private int[][] chrBanksData;

    @Override
    public INesRomData build() {
        INesRomData rom = new INesRomData();
        rom.setHeader(header);
        rom.setTrainerData(trainerData);
        rom.setPrgBanksData(prgBanksData);
        rom.setChrBanksData(chrBanksData);

        return rom;
    }

    public INesRomDataBuilder withHeader(INesRomDataHeader header) {
        this.header = header;
        return this;
    }

    public INesRomDataBuilder withTrainerData(int[] trainerData) {
        this.trainerData = trainerData;
        return this;
    }

    public INesRomDataBuilder withPrgBanksData(int[][] prgBanksData) {
        this.prgBanksData = prgBanksData;
        return this;
    }

    public INesRomDataBuilder withChrBanksData(int[][] chrBanksData) {
        this.chrBanksData = chrBanksData;
        return this;
    }
}
