package piotrkulma.javanes.nesrom.builder.inesrom;

import piotrkulma.javanes.nesrom.builder.NesRomDataBuilder;
import piotrkulma.javanes.nesrom.file.inesrom.INesRomDataHeader;

public class INesRomDataHeaderBuilder implements NesRomDataBuilder<INesRomDataHeader> {
    private String firstFileFormatId;
    private byte secondFileFormatId;
    private int prgRomBanksSize;
    private int chrRomBanksSize;
    private byte romControlByte1;
    private byte romControlByte2;
    private int ramBanksSize;
    private byte reserved;

    @Override
    public INesRomDataHeader build() {
        INesRomDataHeader rom = new INesRomDataHeader();
        rom.setFirstFileFormatId(firstFileFormatId);
        rom.setSecondFileFormatId(secondFileFormatId);
        rom.setPrgRomBanksSize(prgRomBanksSize);
        rom.setChrRomBanksSize(chrRomBanksSize);
        rom.setRomControlByte1(romControlByte1);
        rom.setRomControlByte2(romControlByte2);
        rom.setRamBanksSize(ramBanksSize);
        rom.setReserved(reserved);
        return rom;
    }

    public INesRomDataHeaderBuilder withFirstFileFormatId(String firstFileFormatId) {
        this.firstFileFormatId = firstFileFormatId;
        return this;
    }

    public INesRomDataHeaderBuilder withSecondFileFormatId(byte secondFileFormatId) {
        this.secondFileFormatId = secondFileFormatId;
        return this;
    }

    public INesRomDataHeaderBuilder withPrgRomBanksSize(int prgRomBanksSize) {
        this.prgRomBanksSize = prgRomBanksSize;
        return this;
    }

    public INesRomDataHeaderBuilder withChrRomBanksSize(int chrRomBanksSize) {
        this.chrRomBanksSize = chrRomBanksSize;
        return this;
    }

    public INesRomDataHeaderBuilder withRomControlByte1(byte romControlByte1) {
        this.romControlByte1 = romControlByte1;
        return this;
    }

    public INesRomDataHeaderBuilder withRomControlByte2(byte romControlByte2) {
        this.romControlByte2 = romControlByte2;
        return this;
    }

    public INesRomDataHeaderBuilder withRamBanksSize(int ramBanksSize) {
        this.ramBanksSize = ramBanksSize;
        return this;
    }

    public INesRomDataHeaderBuilder withReserved(byte reserved) {
        this.reserved = reserved;
        return this;
    }
}
