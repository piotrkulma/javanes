package piotrkulma.javanes.nesrom.reader.inesrom;

import org.apache.log4j.Logger;
import piotrkulma.javanes.nesrom.builder.inesrom.INesRomDataBuilder;
import piotrkulma.javanes.nesrom.builder.inesrom.INesRomDataHeaderBuilder;
import piotrkulma.javanes.nesrom.file.inesrom.INesRomData;
import piotrkulma.javanes.nesrom.file.inesrom.INesRomDataHeader;
import piotrkulma.javanes.nesrom.reader.NesRomReader;

import java.io.IOException;
import java.io.InputStream;

import static piotrkulma.javanes.common.BinaryUtils.*;

public final class INesRomReader implements NesRomReader<INesRomData> {
    final static Logger LOG = Logger.getLogger(INesRomReader.class);

    public static INesRomReader getInstance() {
        return new INesRomReader();
    }

    @Override
    public INesRomData readNesFile(String path) {
        INesRomData romData;

        try(InputStream romStream = INesRomReader.class.getClassLoader().getResourceAsStream("smb.nes")) {
            romData = readRom(romStream);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            throw new RuntimeException(String.format("Error while reading iNes file %s: \n%s", path, e.getMessage()));
        }
        return romData;
    }

    private INesRomData readRom(InputStream romStream) throws IOException{
        INesRomDataHeader headerData = readRomHeader(romStream);

        INesRomDataBuilder builder = new INesRomDataBuilder();
        builder.withHeader(headerData);

        if(headerData.isTrainer()) {
            builder.withTrainerData(readBytes(romStream,  INesRomData.TRAINER_SIZE));
        }
        builder.withPrgBanksData(Utils.readDataSectors(romStream, headerData.getPrgRomBanksSize(), INesRomData.PRG_ROM_SIZE));
        builder.withChrBanksData(Utils.readDataSectors(romStream, headerData.getChrRomBanksSize(), INesRomData.CHR_ROM_SIZE));

        INesRomData romData = builder.build();

        LOG.info(String.format("ROM TRAINER DATA: %s", romData.getTrainerData()));
        LOG.info(String.format("ROM PGR BANKS DATA: %s", romData.getPrgBanksData()));
        LOG.info(String.format("ROM CHR BANKS DATA: %s", romData.getChrBanksData()));
        return romData;
    }

    private INesRomDataHeader readRomHeader(InputStream romStream) throws IOException {
        LOG.info("AVAILABLE BYTES: " + romStream.available());

        INesRomDataHeaderBuilder headerBuilder = new INesRomDataHeaderBuilder();

        headerBuilder.withFirstFileFormatId(new String(readRawBytes(romStream, 3)));
        headerBuilder.withSecondFileFormatId(readRawBytes(romStream, 1)[0]);
        headerBuilder.withPrgRomBanksSize(readRawBytes(romStream, 1)[0]);
        headerBuilder.withChrRomBanksSize(readRawBytes(romStream, 1)[0]);
        headerBuilder.withRomControlByte1(readRawBytes(romStream, 1)[0]);
        headerBuilder.withRomControlByte2(readRawBytes(romStream, 1)[0]);
        headerBuilder.withRamBanksSize(readRawBytes(romStream, 1)[0]);
        headerBuilder.withReserved(readRawBytes(romStream, 7)[0]);

        INesRomDataHeader header = headerBuilder.build();
        LOG.info(String.format("ROM HEADER DATA: %s", header));
        return header;
    }

    private INesRomReader() {
    }
}
