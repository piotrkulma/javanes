package piotrkulma.javanes.nesrom.reader.inesrom;

import piotrkulma.javanes.common.BinaryUtils;

import java.io.IOException;
import java.io.InputStream;

public final class Utils {
    public static int[][] readDataSectors(InputStream romStream, int sectorsCount, int sectorSize) throws IOException {
        int[][] romData = new int[sectorsCount][sectorSize];

        for(int i=0; i<sectorsCount; i++) {
            romData[i] = BinaryUtils.readBytes(romStream, sectorSize);
        }

        return romData;
    }

    private Utils() {
    }
}
