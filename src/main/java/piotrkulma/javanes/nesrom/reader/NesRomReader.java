package piotrkulma.javanes.nesrom.reader;

import piotrkulma.javanes.nesrom.file.NesRomData;

public interface NesRomReader<T extends NesRomData> {
    T readNesFile(String path);
}
