package piotrkulma.javanes.nesrom.file.inesrom;

import piotrkulma.javanes.common.BinaryUtils;
import piotrkulma.javanes.nesrom.file.NesRomData;

import static piotrkulma.javanes.common.BinaryUtils.getBit;

public class INesRomDataHeader implements NesRomData {
    private String firstFileFormatId;
    private byte secondFileFormatId;
    private int prgRomBanksSize;
    private int chrRomBanksSize;
    private byte romControlByte1;
    private byte romControlByte2;
    private int ramBanksSize;
    private byte reserved;

    //index 0 of ROM control byte 1, true if 0 value
    private boolean horizontalMirroring;

    //index 0 of ROM control byte 1, true if 1 value
    private boolean verticalMirroring;

    //index 1 of ROM control byte 1
    private boolean batteryBacked;

    //index 2 of ROM control byte 1
    private boolean trainer;


    public String getFirstFileFormatId() {
        return firstFileFormatId;
    }

    public void setFirstFileFormatId(String firstFileFormatId) {
        this.firstFileFormatId = firstFileFormatId;
    }

    public byte getSecondFileFormatId() {
        return secondFileFormatId;
    }

    public void setSecondFileFormatId(byte secondFileFormatId) {
        this.secondFileFormatId = secondFileFormatId;
    }

    public int getPrgRomBanksSize() {
        return prgRomBanksSize;
    }

    public void setPrgRomBanksSize(int prgRomBanksSize) {
        this.prgRomBanksSize = prgRomBanksSize;
    }

    public int getChrRomBanksSize() {
        return chrRomBanksSize;
    }

    public void setChrRomBanksSize(int chrRomBanksSize) {
        this.chrRomBanksSize = chrRomBanksSize;
    }

    public byte getRomControlByte1() {
        return romControlByte1;
    }

    public void setRomControlByte1(byte romControlByte1) {
        this.romControlByte1 = romControlByte1;
        setRomControlByte1SingleBytes();
    }

    public byte getRomControlByte2() {
        return romControlByte2;
    }

    public void setRomControlByte2(byte romControlByte2) {
        this.romControlByte2 = romControlByte2;
    }

    public int getRamBanksSize() {
        return ramBanksSize;
    }

    public void setRamBanksSize(int ramBanksSize) {
        this.ramBanksSize = ramBanksSize;
    }

    public byte getReserved() {
        return reserved;
    }

    public void setReserved(byte reserved) {
        this.reserved = reserved;
    }

    public boolean isHorizontalMirroring() {
        return horizontalMirroring;
    }

    public void setHorizontalMirroring(boolean horizontalMirroring) {
        this.horizontalMirroring = horizontalMirroring;
    }

    public boolean isVerticalMirroring() {
        return verticalMirroring;
    }

    public void setVerticalMirroring(boolean verticalMirroring) {
        this.verticalMirroring = verticalMirroring;
    }

    public boolean isBatteryBacked() {
        return batteryBacked;
    }

    public void setBatteryBacked(boolean batteryBacked) {
        this.batteryBacked = batteryBacked;
    }

    public boolean isTrainer() {
        return trainer;
    }

    public void setTrainer(boolean trainer) {
        this.trainer = trainer;
    }

    @Override
    public String toString() {
        return "INesRomDataHeader{" +
                "firstFileFormatId='" + firstFileFormatId + '\'' +
                ", secondFileFormatId=" +  Integer.toHexString(secondFileFormatId) +
                ", prgRomBanksSize=" + prgRomBanksSize +
                ", chrRomBanksSize=" + chrRomBanksSize +
                ", romControlByte1=" + romControlByte1 +
                ", romControlByte2=" + romControlByte2 +
                ", ramBanksSize=" + ramBanksSize +
                ", reserved=" + reserved +
                ", horizontalMirroring=" + horizontalMirroring +
                ", verticalMirroring=" + verticalMirroring +
                ", batteryBacked=" + batteryBacked +
                ", trainer=" + trainer +
                '}';
    }

    private void setRomControlByte1SingleBytes() {
        if (getBit(romControlByte1, 0) == 0) {
            setHorizontalMirroring(true);
        }

        if (getBit(romControlByte1, 0) == 1) {
            setVerticalMirroring(true);
        }

        if (getBit(romControlByte1, 1) == 1) {
            setBatteryBacked(true);
        }

        if (getBit(romControlByte1, 2) == 1) {
            setTrainer(true);
        }
    }
}
