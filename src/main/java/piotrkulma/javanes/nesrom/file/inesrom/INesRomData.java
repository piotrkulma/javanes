package piotrkulma.javanes.nesrom.file.inesrom;

import piotrkulma.javanes.nesrom.file.NesRomData;

import java.util.Arrays;

public class INesRomData implements NesRomData {
    public static final int TRAINER_SIZE = 512;
    public static final int PRG_ROM_SIZE = 16 * 1024;
    public static final int CHR_ROM_SIZE = 8 * 1024;

    private INesRomDataHeader header;
    private int[] trainerData;
    private int[][] prgBanksData;
    private int[][] chrBanksData;

    public INesRomDataHeader getHeader() {
        return header;
    }

    public void setHeader(INesRomDataHeader header) {
        this.header = header;
    }

    public int[] getTrainerData() {
        return trainerData;
    }

    public void setTrainerData(int[] trainerData) {
        this.trainerData = trainerData;
    }

    public int[][] getPrgBanksData() {
        return prgBanksData;
    }

    public void setPrgBanksData(int[][] romBanksData) {
        this.prgBanksData = romBanksData;
    }

    public int[][] getChrBanksData() {
        return chrBanksData;
    }

    public void setChrBanksData(int[][] chrBanksData) {
        this.chrBanksData = chrBanksData;
    }

    @Override
    public String toString() {
        return "INesRomData{" +
                "header=" + header.toString() +
                ", trainerData=" + Arrays.toString(trainerData) +
                ", prgBanksData=" + Arrays.toString(prgBanksData) +
                ", chrBanksData=" + Arrays.toString(chrBanksData) +
                '}';
    }
}
