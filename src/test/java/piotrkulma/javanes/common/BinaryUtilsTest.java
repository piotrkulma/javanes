package piotrkulma.javanes.common;

import static org.junit.jupiter.api.Assertions.*;
import static piotrkulma.javanes.common.BinaryUtils.*;

import org.junit.jupiter.api.Test;

public class BinaryUtilsTest {

    @Test
    public void getBitTest() {
        byte value = (byte) 0b10011101;

        assertEquals(getBit(value, 0), 1);
        assertEquals(getBit(value, 1), 0);
        assertEquals(getBit(value, 2), 1);
        assertEquals(getBit(value, 3), 1);
        assertEquals(getBit(value, 4), 1);
        assertEquals(getBit(value, 5), 0);
        assertEquals(getBit(value, 6), 0);
        assertEquals(getBit(value, 7), 1);
    }

    @Test
    public void getBinarySizeTest() {
        assertEquals(0, getBinarySize(0b000000));
        assertEquals(1, getBinarySize(0b000001));
        assertEquals(2, getBinarySize(0b000011));
        assertEquals(3, getBinarySize(0b000111));
        assertEquals(4, getBinarySize(0b001111));
        assertEquals(5, getBinarySize(0b011111));
        assertEquals(6, getBinarySize(0b111111));

        assertEquals(6, getBinarySize(0b100000));
        assertEquals(5, getBinarySize(0b010000));
        assertEquals(4, getBinarySize(0b001000));
        assertEquals(3, getBinarySize(0b000100));
        assertEquals(2, getBinarySize(0b000010));
        assertEquals(1, getBinarySize(0b000001));
    }

    @Test
    public void formValueTest() {
        assertEquals(0xDEAD, formTwoBytesValue(0xAD, 0xDE));
        assertEquals(0xBBFF, formTwoBytesValue(0xFF, 0xBB));
        assertEquals(0xCDAB, formTwoBytesValue(0xAB, 0xCD));
        assertEquals(0xFF00, formTwoBytesValue(0x00, 0xFF));
        assertEquals(0x00FF, formTwoBytesValue(0xFF, 0x00));
        assertEquals(0x0000, formTwoBytesValue(0x00, 0x00));
        assertEquals(0x1001, formTwoBytesValue(0x01, 0x10));
        assertEquals(0x0110, formTwoBytesValue(0x10, 0x01));
        assertEquals(0b1100111101100111, formTwoBytesValue(0b01100111, 0b11001111));
    }
}
