package piotrkulma.javanes.cpu.memory;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import piotrkulma.javanes.nesrom.file.inesrom.INesRomData;
import piotrkulma.javanes.nesrom.reader.NesRomReader;
import piotrkulma.javanes.nesrom.reader.inesrom.INesRomReader;

import static piotrkulma.javanes.cpu.memory.Flag.*;

public class MemoryTest {
    @Test
    public void setMemoryValueLowerMirroringTest() {
        byte value = (byte) 0xFE;

        Memory memory = getMemory();
        memory.setMemoryValue(0x0000, value);
        assertEquals(memory.getMemoryValue(0x0000), value);
        assertEquals(memory.getMemoryValue(0x0800), value);
        assertEquals(memory.getMemoryValue(0x1000), value);
        assertEquals(memory.getMemoryValue(0x1800), value);
        assertEquals(4, countValueInMemory(memory, value));

        memory = getMemory();
        memory.setMemoryValue(0x0222, value);
        assertEquals(memory.getMemoryValue(0x0222), value);
        assertEquals(memory.getMemoryValue(0x0800 + 0x0222), value);
        assertEquals(memory.getMemoryValue(0x1000 + 0x0222), value);
        assertEquals(memory.getMemoryValue(0x1800 + 0x0222), value);
        assertEquals(4, countValueInMemory(memory, value));

        memory = getMemory();
        memory.setMemoryValue(0x0122, value);
        assertEquals(memory.getMemoryValue(0x0122), value);
        assertEquals(memory.getMemoryValue(0x0800 + 0x0122), value);
        assertEquals(memory.getMemoryValue(0x1000 + 0x0122), value);
        assertEquals(memory.getMemoryValue(0x1800 + 0x0122), value);
        assertEquals(4, countValueInMemory(memory, value));

        memory = getMemory();
        memory.setMemoryValue(0x0800, value);
        assertEquals(memory.getMemoryValue(0x0800), value);
        assertEquals(1, countValueInMemory(memory, value));
        for(int i=0; i<Memory.CPU_MEMORY_SIZE; i++) {
            if(i != 0x0800) {
                assertEquals(0x0000, memory.getMemoryValue(i));
            }
        }
    }

    @Test
    public void setMemoryValueUpperMirroringTest() {
        byte value = (byte) 0xFE;

        Memory memory = getMemory();
        memory.setMemoryValue(0x02001, value);
        assertEquals(memory.getMemoryValue(0x02001), value);
        assertEquals((8184/8)+1, countValueInMemory(memory, value));

        memory = getMemory();
        memory.setMemoryValue(0x2007, value);
        assertEquals(memory.getMemoryValue(0x2007), value);
        assertEquals((8184/8)+1, countValueInMemory(memory, value));
        for(int i=0x2008 + 7; i<0x4000; i+=8) {
            assertEquals(value, memory.getMemoryValue(i));
        }

        memory = getMemory();
        memory.setMemoryValue(0x02008, value);
        assertEquals(memory.getMemoryValue(0x2008), value);
        assertEquals(1, countValueInMemory(memory, value));
        for(int i=0; i<Memory.CPU_MEMORY_SIZE; i++) {
            if(i != 0x2008) {
                assertEquals(0x0000, memory.getMemoryValue(i));
            }
        }
    }

    @Test
    public void testPrgRomBanksSizeOne() {
        NesRomReader<INesRomData> reader = INesRomReader.getInstance();
        INesRomData iNesRomData = reader.readNesFile("xxx.y");
        Memory memory = getMemory();

        Utils.loadProgramIntoMemory(memory, iNesRomData.getPrgBanksData(), 1);

        for(int i=0; i<iNesRomData.getPrgBanksData()[0].length; i++) {
            assertEquals(iNesRomData.getPrgBanksData()[0][i], memory.getMemoryValue(0x8000 + i));
        }

        for(int i=0; i<iNesRomData.getPrgBanksData()[0].length; i++) {
            assertEquals(iNesRomData.getPrgBanksData()[0][i], memory.getMemoryValue(0xC000 + i));
        }
    }

    @Test
    public void testPrgRomBanksSizeTwo() {
        NesRomReader<INesRomData> reader = INesRomReader.getInstance();
        INesRomData iNesRomData = reader.readNesFile("xxx.y");
        Memory memory = getMemory();

        Utils.loadProgramIntoMemory(memory, iNesRomData.getPrgBanksData(), 2);

        for(int i=0; i<iNesRomData.getPrgBanksData()[0].length; i++) {
            assertEquals(iNesRomData.getPrgBanksData()[0][i], memory.getMemoryValue(0x8000 + i));
        }

        for(int i=0; i<iNesRomData.getPrgBanksData()[1].length; i++) {
            assertEquals(iNesRomData.getPrgBanksData()[1][i], memory.getMemoryValue(0xC000 + i));
        }
    }

    @Test
    public void flagsTest() {
        assertEquals(C.getIndex(), 0);
        assertEquals(Z.getIndex(), 1);
        assertEquals(I.getIndex(), 2);
        assertEquals(D.getIndex(), 3);
        assertEquals(B.getIndex(), 4);
        assertEquals(V.getIndex(), 6);
        assertEquals(N.getIndex(), 7);
    }

    private int countValueInMemory(Memory memory, byte value) {
        int count = 0;
        for(int i=0; i<Memory.CPU_MEMORY_SIZE; i++) {
            if(memory.getMemoryValue(i) == value) {
                count ++;
            }
        }

        return count;
    }

    private Memory getMemory() {
        return new Memory();
    }
}
