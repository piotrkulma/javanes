package piotrkulma.javanes.cpu.opcode.executor;

import org.junit.jupiter.api.Test;
import piotrkulma.javanes.cpu.memory.Memory;
import piotrkulma.javanes.cpu.opcode.OperationCodeData;
import piotrkulma.javanes.cpu.opcode.OperationCodeDataBuilder;

import static piotrkulma.javanes.cpu.memory.Flag.*;
import static piotrkulma.javanes.cpu.opcode.executor.Utils.*;
import static org.junit.jupiter.api.Assertions.*;

public class UtilsTest {
    @Test
    public void isAffectedFlagTest() {
        //NV-BDIZC
        String flag = "--------";
        assertEquals(false, isAffectedFlag(N, flag));
        assertEquals(false, isAffectedFlag(V, flag));
        assertEquals(false, isAffectedFlag(B, flag));
        assertEquals(false, isAffectedFlag(D, flag));
        assertEquals(false, isAffectedFlag(I, flag));
        assertEquals(false, isAffectedFlag(Z, flag));
        assertEquals(false, isAffectedFlag(C, flag));

        flag = "NV-BDIZC";
        assertEquals(true, isAffectedFlag(N, flag));
        assertEquals(true, isAffectedFlag(V, flag));
        assertEquals(true, isAffectedFlag(B, flag));
        assertEquals(true, isAffectedFlag(D, flag));
        assertEquals(true, isAffectedFlag(I, flag));
        assertEquals(true, isAffectedFlag(Z, flag));
        assertEquals(true, isAffectedFlag(C, flag));

        flag = "N-------";
        assertEquals(true, isAffectedFlag(N, flag));
        assertEquals(false, isAffectedFlag(V, flag));
        assertEquals(false, isAffectedFlag(B, flag));
        assertEquals(false, isAffectedFlag(D, flag));
        assertEquals(false, isAffectedFlag(I, flag));
        assertEquals(false, isAffectedFlag(Z, flag));
        assertEquals(false, isAffectedFlag(C, flag));

        flag = "-V------";
        assertEquals(false, isAffectedFlag(N, flag));
        assertEquals(true, isAffectedFlag(V, flag));
        assertEquals(false, isAffectedFlag(B, flag));
        assertEquals(false, isAffectedFlag(D, flag));
        assertEquals(false, isAffectedFlag(I, flag));
        assertEquals(false, isAffectedFlag(Z, flag));
        assertEquals(false, isAffectedFlag(C, flag));

        flag = "---B----";
        assertEquals(false, isAffectedFlag(N, flag));
        assertEquals(false, isAffectedFlag(V, flag));
        assertEquals(true, isAffectedFlag(B, flag));
        assertEquals(false, isAffectedFlag(D, flag));
        assertEquals(false, isAffectedFlag(I, flag));
        assertEquals(false, isAffectedFlag(Z, flag));
        assertEquals(false, isAffectedFlag(C, flag));

        flag = "----D---";
        assertEquals(false, isAffectedFlag(N, flag));
        assertEquals(false, isAffectedFlag(V, flag));
        assertEquals(false, isAffectedFlag(B, flag));
        assertEquals(true, isAffectedFlag(D, flag));
        assertEquals(false, isAffectedFlag(I, flag));
        assertEquals(false, isAffectedFlag(Z, flag));
        assertEquals(false, isAffectedFlag(C, flag));

        flag = "-----I--";
        assertEquals(false, isAffectedFlag(N, flag));
        assertEquals(false, isAffectedFlag(V, flag));
        assertEquals(false, isAffectedFlag(B, flag));
        assertEquals(false, isAffectedFlag(D, flag));
        assertEquals(true, isAffectedFlag(I, flag));
        assertEquals(false, isAffectedFlag(Z, flag));
        assertEquals(false, isAffectedFlag(C, flag));

        flag = "------Z-";
        assertEquals(false, isAffectedFlag(N, flag));
        assertEquals(false, isAffectedFlag(V, flag));
        assertEquals(false, isAffectedFlag(B, flag));
        assertEquals(false, isAffectedFlag(D, flag));
        assertEquals(false, isAffectedFlag(I, flag));
        assertEquals(true, isAffectedFlag(Z, flag));
        assertEquals(false, isAffectedFlag(C, flag));

        flag = "-------C";
        assertEquals(false, isAffectedFlag(N, flag));
        assertEquals(false, isAffectedFlag(V, flag));
        assertEquals(false, isAffectedFlag(B, flag));
        assertEquals(false, isAffectedFlag(D, flag));
        assertEquals(false, isAffectedFlag(I, flag));
        assertEquals(false, isAffectedFlag(Z, flag));
        assertEquals(true, isAffectedFlag(C, flag));
    }

    public void affectsFlagsTest(){
        Memory memory = new Memory();
        affectsFlags(memory, "-V-----C", 0x01 + 0x01);
        assertEquals(0, memory.getProcesorStatusByte(C));
        assertEquals(0, memory.getProcesorStatusByte(V));

        memory = new Memory();
        affectsFlags(memory, "-V-----C", 0x01 + 0xFF);
        assertEquals(1, memory.getProcesorStatusByte(C));
        assertEquals(0, memory.getProcesorStatusByte(V));

        memory = new Memory();
        affectsFlags(memory, "-V-----C", 0x7F + 0x01);
        assertEquals(0, memory.getProcesorStatusByte(C));
        assertEquals(1, memory.getProcesorStatusByte(V));

        memory = new Memory();
        affectsFlags(memory, "-V-----C", 0x80 + 0xFF);
        assertEquals(1, memory.getProcesorStatusByte(C));
        assertEquals(1, memory.getProcesorStatusByte(V));

        memory = new Memory();
        affectsFlags(memory, "N-------", 0b10000000);
        assertEquals(1, memory.getProcesorStatusByte(N));

        memory = new Memory();
        affectsFlags(memory, "N-------", 0b01000001);
        assertEquals(0, memory.getProcesorStatusByte(N));

        memory = new Memory();
        affectsFlags(memory, "N-------", 0b11000001);
        assertEquals(1, memory.getProcesorStatusByte(N));

        memory = new Memory();
        affectsFlags(memory, "N-------", 0b01111111);
        assertEquals(0, memory.getProcesorStatusByte(N));

        memory = new Memory();
        affectsFlags(memory, "N-------", 0b01111000);
        assertEquals(0, memory.getProcesorStatusByte(Z));

        memory = new Memory();
        affectsFlags(memory, "N-------", 0b00000000);
        assertEquals(1, memory.getProcesorStatusByte(Z));
    }

    private OperationCodeData getOperationCodeDataWithFlags(String flag) {
        OperationCodeDataBuilder builder = new OperationCodeDataBuilder();
        builder.withFlags(flag);

        return builder.build();
    }
}
