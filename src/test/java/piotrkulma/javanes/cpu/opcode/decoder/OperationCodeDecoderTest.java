package piotrkulma.javanes.cpu.opcode.decoder;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import piotrkulma.javanes.cpu.opcode.AddressingMode;
import piotrkulma.javanes.cpu.opcode.OperationCode;
import piotrkulma.javanes.cpu.opcode.OperationCodeData;

import static piotrkulma.javanes.cpu.opcode.OperationCode.*;
import static piotrkulma.javanes.cpu.opcode.AddressingMode.*;

import java.io.IOException;

public class OperationCodeDecoderTest {
    private static OperationCodeDecoder decoder;

    @BeforeAll
    public static void init() {
        try {
            decoder = OperationCodeDecoder.getInstance();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void decodeTest() {
        assertOpCodeEquals(decoder.decode((byte) 0x0A), ASL, ACC, 1);
        assertOpCodeEquals(decoder.decode((byte) 0x81), STA, INDX, 2);
        assertOpCodeEquals(decoder.decode((byte) 0x8D), STA, ABS, 3);

        assertThrows(RuntimeException.class, () -> assertEquals(decoder.decode((byte) 0x80), null));
        assertThrows(RuntimeException.class, () -> assertEquals(decoder.decode((byte) 0x82), null));
    }

    private void assertOpCodeEquals(OperationCodeData ret, OperationCode operationCode, AddressingMode addressingMode, int bytes) {
        assertEquals(ret.getOperationCode(), operationCode);
        assertEquals(ret.getAddressingMode(), addressingMode);
        assertEquals(ret.getBytes(), bytes);
    }
}
